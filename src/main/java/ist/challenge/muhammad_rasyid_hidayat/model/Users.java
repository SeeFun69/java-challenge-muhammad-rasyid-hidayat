package ist.challenge.muhammad_rasyid_hidayat.model;

import ist.challenge.muhammad_rasyid_hidayat.dto.UsersResponseDTO;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name = "users")
@Data
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    private String username;
    private String password;


    public UsersResponseDTO convertToResponse(){
        return UsersResponseDTO.builder()
                .userId(userId)
                .username(username)
                .password(password)
                .build();
    }
}

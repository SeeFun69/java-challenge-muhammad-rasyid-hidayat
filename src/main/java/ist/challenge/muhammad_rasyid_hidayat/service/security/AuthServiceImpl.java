package ist.challenge.muhammad_rasyid_hidayat.service.security;

import ist.challenge.muhammad_rasyid_hidayat.controller.AuthController;
import ist.challenge.muhammad_rasyid_hidayat.dto.*;
import ist.challenge.muhammad_rasyid_hidayat.exception.ResourceNotFoundException;
import ist.challenge.muhammad_rasyid_hidayat.model.Users;
import ist.challenge.muhammad_rasyid_hidayat.repo.UsersRepo;
import ist.challenge.muhammad_rasyid_hidayat.response.ResponseHandler;
import ist.challenge.muhammad_rasyid_hidayat.service.UsersService;
import ist.challenge.muhammad_rasyid_hidayat.service.jwt.JwtUtils;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class AuthServiceImpl implements AuthService {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UsersRepo userRepo;
    @Autowired
    UsersService usersService;
    @Autowired
    JwtUtils jwtUtils;

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
    @Override
    public ResponseEntity<Object> signup(SignupRequest request){

        try {
            if(request.getPassword().isEmpty() || request.getUsername().isEmpty()){
                throw new ResourceNotFoundException("Complete all field");
            }

            if (userRepo.existsByUsername(request.getUsername())) {
                throw new Exception("Username already taken!");
            }

            Users users = new Users();
            users.setUsername(request.getUsername());
            users.setPassword(request.getPassword());
            usersService.createUsers(users);
            UsersResponseDTO userResult = users.convertToResponse();
            logger.info("------------------------------------");
            logger.info("User created: " + userResult);
            logger.info("------------------------------------");
            return ResponseHandler.generateResponse("Successfully Created User!", HttpStatus.CREATED, userResult);
        } catch (Exception e) {
            logger.error("------------------------------------");
            logger.error(e.getMessage());
            logger.error("------------------------------------");
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.BAD_REQUEST, "Sign Up failed");
        }
    }

    @Override
    public ResponseEntity<JwtResponse> authenticateUser(LoginRequest request) {
        if(request.getPassword().isEmpty() || request.getUsername().isEmpty()){
            throw new ResourceNotFoundException("Username and Password can't be null");
        }

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        String token = jwtUtils.generateJwtToken(authentication);
        UserDetailsImpl principal = (UserDetailsImpl) authentication.getPrincipal();
        return ResponseEntity.ok().body(new JwtResponse(token, principal.getUsername()));

    }
}
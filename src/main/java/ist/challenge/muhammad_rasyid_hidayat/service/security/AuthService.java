package ist.challenge.muhammad_rasyid_hidayat.service.security;

import ist.challenge.muhammad_rasyid_hidayat.dto.JwtResponse;
import ist.challenge.muhammad_rasyid_hidayat.dto.LoginRequest;
import ist.challenge.muhammad_rasyid_hidayat.dto.SignupRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface AuthService {
    ResponseEntity<Object> signup(@RequestBody SignupRequest request);
    ResponseEntity<JwtResponse> authenticateUser(@RequestBody LoginRequest request);
}

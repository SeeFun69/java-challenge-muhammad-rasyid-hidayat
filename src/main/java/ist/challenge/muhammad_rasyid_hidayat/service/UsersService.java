package ist.challenge.muhammad_rasyid_hidayat.service;

import ist.challenge.muhammad_rasyid_hidayat.dto.PassworRequest;
import ist.challenge.muhammad_rasyid_hidayat.dto.UsersRequestDTO;
import ist.challenge.muhammad_rasyid_hidayat.model.Users;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UsersService {
    ResponseEntity<Object> getAll();
    Users createUsers(Users users);
    ResponseEntity<Object> deleteUserById(Long users_Id);
    ResponseEntity<Object> update(UsersRequestDTO requestDTO);
}

package ist.challenge.muhammad_rasyid_hidayat.service;

import ist.challenge.muhammad_rasyid_hidayat.dto.PassworRequest;
import ist.challenge.muhammad_rasyid_hidayat.dto.UsersRequestDTO;
import ist.challenge.muhammad_rasyid_hidayat.dto.UsersResponseDTO;
import ist.challenge.muhammad_rasyid_hidayat.exception.ResourceNotFoundException;
import ist.challenge.muhammad_rasyid_hidayat.model.Users;
import ist.challenge.muhammad_rasyid_hidayat.repo.UsersRepo;
import ist.challenge.muhammad_rasyid_hidayat.response.ResponseHandler;
import ist.challenge.muhammad_rasyid_hidayat.service.security.UserDetailsImpl;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class UsersServiceImpl implements UsersService {
    @Autowired
    private UsersRepo usersRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;
    private static final Logger logger = LogManager.getLogger(UsersServiceImpl.class);

    @Override
    public ResponseEntity<Object> getAll() {
        List<Users> users = usersRepo.findAll();
        if(users.isEmpty()){
            throw new ResourceNotFoundException("User not exist with id :");
        }
        try {
            List<Users> result = usersRepo.findAll();
            List<UsersResponseDTO> usersResponseDTOList = new ArrayList<>();
            logger.info("==================== Logger Start Get All User ====================");
            for (Users dataresult:result){
                Map<String, Object> order = new HashMap<>();
                order.put("id_akun         : ", dataresult.getUserId());
                order.put("nama            : ", dataresult.getUsername());
                order.put("password           : ", dataresult.getPassword());

                logger.info("id_akun       : " + dataresult.getUserId());
                logger.info("nama          : " + dataresult.getUsername());
                logger.info("password         : " + dataresult.getPassword());
                UsersResponseDTO usersResponseDTO = dataresult.convertToResponse();
                usersResponseDTOList.add(usersResponseDTO);

                logger.info("==================== Logger End Get All User ====================");
            }
            return ResponseHandler.generateResponse("Successfully Get All User!", HttpStatus.OK, usersResponseDTOList);
        } catch (Exception e) {
            logger.error("------------------------------------");
            logger.error(e.getMessage());
            logger.error("------------------------------------");
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "Bad Request!!");
        }
    }

    @Override
    public ResponseEntity<Object> deleteUserById(Long users_Id) {
        try {
            Optional<Users> optionalUser = usersRepo.findById(users_Id);
            if(optionalUser.isEmpty()){
                throw new ResourceNotFoundException("User not exist with id :" + users_Id);
            }
            usersRepo.deleteById(users_Id);
            Map<String, Boolean> response = new HashMap<>();
            response.put("Delete Status", Boolean.TRUE);
            logger.info("==================== Logger Start Hard Delete User By ID ====================");
            logger.info(response);
            logger.info("==================== Logger End Hard Delete User By ID =================");
            return ResponseHandler.generateResponse("Successfully Delete User! ", HttpStatus.OK, response);
        } catch (ResourceNotFoundException e){
            logger.error("------------------------------------");
            logger.error(e.getMessage());
            logger.error("------------------------------------");
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.NOT_FOUND, "Data Not Found!" );
        }
    }

    @Override
    public ResponseEntity<Object> update(UsersRequestDTO requestDTO) {
        try {
            UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (usersRepo.existsByUsername(requestDTO.getUsername())) {
                throw new Exception("Username already taken!");
            }
            if (passwordEncoder.matches(requestDTO.getPassword(), userDetails.getPassword())) {
                throw new ResourceNotFoundException("Password is same");
            }
            Users users = requestDTO.convertToEntity();
            users.setUserId(userDetails.getUserId());
            users.setPassword(passwordEncoder.encode(requestDTO.getPassword()));
            Users updateUsers = usersRepo.save(users);
            UsersResponseDTO result = updateUsers.convertToResponse();
            logger.info("==================== Logger Start Update User By ID ====================");
            logger.info(result);
            logger.info("==================== Logger End Update User By ID =================");
            return ResponseHandler.generateResponse("Successfully Updated User. Please login again.",HttpStatus.OK, result);
        }catch(Exception e){
            logger.error("------------------------------------");
            logger.error(e.getMessage());
            logger.error("------------------------------------");
            return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.NOT_FOUND,"Bad Request!!");
        }
    }
    @Override
    public Users createUsers(Users users) {
        users.setPassword(passwordEncoder.encode(users.getPassword()));
        return this.usersRepo.save(users);
    }
}

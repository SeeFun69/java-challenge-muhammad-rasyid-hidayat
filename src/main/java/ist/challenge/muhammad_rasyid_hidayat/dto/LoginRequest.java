package ist.challenge.muhammad_rasyid_hidayat.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginRequest implements Serializable {
    private String username;
    private String password;


}

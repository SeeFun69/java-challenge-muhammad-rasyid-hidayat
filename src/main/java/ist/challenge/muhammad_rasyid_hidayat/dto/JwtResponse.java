package ist.challenge.muhammad_rasyid_hidayat.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class JwtResponse implements Serializable {
    private String token;
    private String type = "Bearer";
    private String username;
    public JwtResponse(
            String accessToken,
            String username) {
        this.username = username;
        this.token = accessToken;
    }

}

package ist.challenge.muhammad_rasyid_hidayat.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersResponseDTO {
    private Long userId;
    private String username;
    private String password;

    @Override
    public String toString() {
        return "UsersResponseDTO{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}

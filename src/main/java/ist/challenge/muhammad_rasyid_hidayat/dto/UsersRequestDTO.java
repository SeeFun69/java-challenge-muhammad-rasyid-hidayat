package ist.challenge.muhammad_rasyid_hidayat.dto;
import ist.challenge.muhammad_rasyid_hidayat.model.Users;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsersRequestDTO {
    private String username;
    private String password;


    public Users convertToEntity(){
        return Users.builder()
                .username(this.username)
                .password(this.password)
                .build();
    }
}

package ist.challenge.muhammad_rasyid_hidayat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MuhammadRasyidHidayatApplication {

	public static void main(String[] args) {
		SpringApplication.run(MuhammadRasyidHidayatApplication.class, args);
	}

}

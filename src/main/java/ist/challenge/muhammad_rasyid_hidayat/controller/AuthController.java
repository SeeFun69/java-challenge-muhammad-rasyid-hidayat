package ist.challenge.muhammad_rasyid_hidayat.controller;

import io.swagger.v3.oas.annotations.Operation;
import ist.challenge.muhammad_rasyid_hidayat.dto.JwtResponse;
import ist.challenge.muhammad_rasyid_hidayat.dto.LoginRequest;
import ist.challenge.muhammad_rasyid_hidayat.dto.SignupRequest;
import ist.challenge.muhammad_rasyid_hidayat.service.security.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    AuthService authService;

    @Operation(summary = "Login for get the token access")
    @PostMapping("/login")
    public ResponseEntity<JwtResponse> authenticateUser(@RequestBody LoginRequest request) {
        return authService.authenticateUser(request);
        }

    @Operation(summary = "Sign Up for new User")
    @PostMapping("/signup")
    public ResponseEntity<Object> signup(@RequestBody SignupRequest request) {

        return authService.signup(request);
    }

}

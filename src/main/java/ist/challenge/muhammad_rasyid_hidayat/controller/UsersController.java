package ist.challenge.muhammad_rasyid_hidayat.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import ist.challenge.muhammad_rasyid_hidayat.dto.PassworRequest;
import ist.challenge.muhammad_rasyid_hidayat.dto.UsersRequestDTO;
import ist.challenge.muhammad_rasyid_hidayat.service.UsersService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/java-challenge/rasyid")
@SecurityRequirement(name = "bearer-key")
@PreAuthorize("isAuthenticated")
public class UsersController {

    @Autowired
    UsersService usersService;

    @GetMapping("/users/all")
    public ResponseEntity <Object> getAll() {
        return usersService.getAll();
    }

    @DeleteMapping("/users/delete_user/{users_Id}")
    public ResponseEntity<Object> deactiveUser(@PathVariable Long users_Id){
       return usersService.deleteUserById(users_Id);
    }

    @PutMapping("/users/{users_Id}")
    public ResponseEntity<Object> updateUser(@RequestBody UsersRequestDTO usersRequestDTO){
        return usersService.update(usersRequestDTO);
    }
}
